"use strict";

module.exports = Franz => {
  const getMessages = function getMessages() {
    let directMessages = 0;
    let indirectMessages = 0;

    // Read the inbox message count in the navbar.
    const messagesElement = document.querySelector(".header-bar-navbar-right .nav-item a[href*='/inbox'] .notification-badge");
    if (messagesElement && messagesElement.innerHTML.length) {
      directMessages = parseInt(messagesElement.innerHTML, 10);
    }

    // Read the notification count in the navbar.
    const notificationElement = document.querySelector(".header-bar-navbar-right .nav-item").querySelector('.notification-badge');
    if (notificationElement && notificationElement.innerHTML.length) {
      indirectMessages += parseInt(notificationElement.innerHTML, 10);
    }

    // If there are new posts to display, flag them as a notification.
    const newPostsElement = document.querySelector(".see-new-posts-text");
    if (newPostsElement && newPostsElement.innerHTML.length) {
      // Text is /See \d+ new posts?/
      indirectMessages += parseInt(newPostsElement.innerHTML.match(/\d+/)[0], 10);
    }

    Franz.setBadge(directMessages, indirectMessages);
  };

  Franz.loop(getMessages);
};
