# Franz Recipe for Nextdoor

This is the unofficial [Franz 5](https://meetfranz.com/) recipe plugin for
[Nextdoor](https://nextdoor.com/).

## Installation

1. Clone/download the folder `franz-recipe-nextdoor`.

2. Open the Franz Recipe folder on your machine:
   * Mac: `~/Library/Application Support/Franz/recipes/`
   * Windows: `%appdata%/Roaming/Franz/recipes/`
   * Linux: `~/.config/Franz/recipes/`

3. Create a `dev` folder if you have not already done so.

4. Copy the `franz-recipe-nextdoor` folder into the recipes dev directory.

5. Restart Franz.
